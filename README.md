# Demo SAKAI 12.0 con docker-compose

## Prerequisitos

1. Instalar Docker y Docker-compose

https://docs.docker.com/get-started/
https://docs.docker.com/compose/install/


2. Clonar sakai desde git clone https://github.com/sakaiproject/sakai.git

```
cd sakai-src
git clone https://github.com/sakaiproject/sakai.git
git checkout tags/12.0
```

3. Documentacion de imagenes utilizadas


https://github.com/mysql/mysql-docker
https://hub.docker.com/_/nginx/

## Crear Imágenes


```
sudo docker-compose build
```

## Levantar contenedores

```
sudo docker-compose up -d --scale sakai-src=1
```

## Detener contenedores

```
sudo docker-compose stop
```

## Remover contenedores

```
sudo docker-compose rm
```

## Reiniciar Nginx

```
sudo docker exec sakaidocker_nginx_1  nginx -s reload
```

